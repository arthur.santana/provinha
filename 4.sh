#!/bin/bash

echo "Alternativa 1 - Margenta(Rosa)"
echo "Alternativa 2 - Branco"
echo "Alternativa 3 - Azul"
read alternativa

test $alternativa == 1 && PS1='\[\033[01;45m\]\u@\h:\w\$ '
test $alternativa == 2 && PS1='\[\033[01;37m\]\u@\h:\w\$ '
test $alternativa == 3 && PS1='\[\033[01;41m\]\u@\h:\w\$ '

