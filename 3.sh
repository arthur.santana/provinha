#!/bin/bash

nome=$1

diretorio=$(ls "/tmp/$nome" "/etc/$nome" "./$nome" 2>/dev/null)

test "$diretorio" != "" && echo "$diretorio" || echo "Não existe!"
